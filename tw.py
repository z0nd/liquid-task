import time
import arrow
from pprint import pprint
from itertools import cycle
from nanpy import Lcd
from taskw import TaskWarrior
from nanpy import SerialManager, ArduinoApi



lcd = Lcd([8, 9, 4, 5, 6, 7], [16, 2])

def main():
    scrollText(clock, get_task)


def clock():
    now = arrow.now()
    _date = now.format("DD/MM")
    _now = now.format("HH:mm")
    _zone = now.format("Z")[:3]
    return f"{_date}  {_now} {_zone}"

def worth_task(task):
    key = ("start" in task, task["urgency"], task["id"])
    return key

def get_task():
    w = TaskWarrior()
    tasks = w.load_tasks()
    pprint(tasks)
    if tasks["pending"]:
        tasks = sorted(tasks["pending"], key=worth_task, reverse=True)[0]
        return f'{tasks["id"]}. {tasks["description"]}'
    else:
        return "no tasks"

def scrollText(clock, task):
    while True:
        message = task()
        if len(message) > 16:
            padding = " " * 16
            oldText = message
            message = padding + message + " "
            for i in range (0, len(message)):
                lcd.setCursor(0,0)
                lcd.printString(clock())
                lcd.setCursor(0,1)
                lcd.printString(message[i:(i+16)], 0, 1)
                time.sleep(0.5)
        else:
            lcd.setCursor(0,0)
            lcd.printString(clock())
            lcd.setCursor(0,1)
            lcd.printString(message, 0, 1)
            time.sleep(0.5)

if __name__ == "__main__":
#    connection = SerialManager(device='/dev/ttyUSB0')
#    arduino = ArduinoApi(connection)
    main()
