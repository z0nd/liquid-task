'''
LcdUi.py - API for easy UI creation using Arduino + LCD + buttons + nanpy
(c)2018 Zeev Kotzer
'''
from __future__ import print_function
import threading
import time
import logging
import bisect
import sys
import Queue

# Pass this as serial port to debug your app in the console with no Arduino connected
SIMULATED_ARDUINO_VIRTUAL_PORT = '::SIMULATE::'

from nanpy import SerialManager, ArduinoApi
from nanpy.lcd import Lcd

# self.menu_sel_char = '>' # chr(7)


class CustomCharsLcd(object):
    """
    Wrapper around nanpy.lcd.Lcd
    """
    MAX_CUSTOM_CHARS = 7

    def __init__(self, pins, begin, connection=None):
        self._lcd = Lcd(pins, begin, connection)
        self._last_char = self.MAX_CUSTOM_CHARS

    def alloc_custom_char_code(self):
        if self._last_char == 0:
            raise RuntimeError('Too many Custom Characters in use - please us up to {}'.format(self.MAX_CHARS))

        self._last_char -= 1

        return self._last_char

    def printString(self, value, col = None, row = None):
        self._lcd.printString(value, col, row)

    def setCursor(self, col, row):
        self._lcd.setCursor(col, row)

    def autoscroll(self):
        self._lcd.autoscroll()

    def noAutoscroll(self):
        self._lcd.noAutoscroll()

    def clear(self):
        self._lcd.clear()

    def createChar(self, num, data):
        self._lcd.createChar(num, data)

    def write(self, data):
        self._lcd.write(data)


class CustomChar(object):
    """
    A custom bit-mapped LCD character
    """

    def __init__(self, fallback_char, pattern_bytes=None):
        self.created = False
        self.lcd = None
        self._pattern_bytes = pattern_bytes
        self.char_code = None
        self.fallback_char = fallback_char
        self.fallback = False

        if not pattern_bytes:  # If represents a regular char
            self.fallback = True
            self.created = False
            return

    def create(self, lcd):
        if self.created:
            if self.lcd is None:
                return
            assert lcd == self.lcd, "Can't create the same chars on more than one Lcd device"
            return

        self.lcd = lcd
        self.char_code = lcd.alloc_custom_char_code()
        if self.char_code < 0:
            raise RuntimeError('Should not happen')

        try:
            assert self._pattern_bytes is not None
            lcd.createChar(self.char_code, self._pattern_bytes)
            self.fallback = False
        except:
            self.fallback = True

        self.created = True

    def get_char(self):
        assert self.created, 'Char {} not created - need to call LcdUI.create_custom_chars(...)'.format(repr(self))
        return self.fallback_char if self.fallback else chr(self.char_code)

    def __str__(self):
        return self.get_char()

    def __repr__(self):
        return '[{}Custom Char "{}"]'.format('' if self.created else 'NOT CREATED ', self.fallback_char)


class LcdButtonDecoder(object):
    ANALOG_INPUT = 0

    BTN_UNKNOWN = 0
    BTN_RIGHT = 30
    BTN_UP = 150
    BTN_DOWN = 360
    BTN_LEFT = 535
    BTN_SELECT = 760
    BTN_NONE = sys.maxint

    _BUTTON_LEVELS = [BTN_UNKNOWN, BTN_RIGHT, BTN_UP, BTN_DOWN, BTN_LEFT, BTN_SELECT, BTN_NONE]  # [30, 150, 360, 535, 760]

    REPEAT_DELAY = 0.8
    REPEAT_FREQ = 0.25

    @staticmethod
    def decode_button(n):
        # find the next button value
        res = n if n in LcdButtonDecoder._BUTTON_LEVELS else \
            LcdButtonDecoder._BUTTON_LEVELS[ bisect.bisect(LcdButtonDecoder._BUTTON_LEVELS, n) ]
        return LcdButtonDecoder.BTN_NONE if res == LcdButtonDecoder.BTN_UNKNOWN else res

    def __init__(self, arduino):
        self.arduino = arduino
        self.last_reading = self.BTN_NONE
        self.last_reading_time = time.time()
        self.last_press_time = None
        self.last_repeat_time = None

    def read_button(self):
        reading = self.arduino.analogRead(self.ANALOG_INPUT)
        t = time.time()
        # time_since_last_read = t - self.last_reading_time
        self.last_reading_time = t

        btn = self.decode_button(reading)
        if btn == self.BTN_NONE:
            self.last_press_time = None
            self.last_reading = btn
            return btn

        # # We have a key press
        if btn != self.last_reading:
            self.last_press_time = t
            self.last_repeat_time = None
            self.last_reading = btn
            return btn

        # We have a repeated reading
        if self.last_repeat_time:
            # If already repeating - check if need to repeat again
            if t - self.last_repeat_time >= self.REPEAT_FREQ:
                self.last_repeat_time = t
                return btn
            else:
                return self.BTN_NONE

        # Check if we need to start repeating
        if t - self.last_press_time >= self.REPEAT_DELAY:
            self.last_repeat_time = t
            return btn

        return self.BTN_NONE


class _VirtualLCD(object):
    def __init__(self, conn=None):
        pass

    def alloc_custom_char_code(self):
        return 5

    def printString(self, value, col = None, row = None):
        if row == col == 0:
            print('\n\n')
        print(value)

    def setCursor(self, col, row):
        pass

    def autoscroll(self):
        pass

    def noAutoscroll(self):
        pass

    def clear(self):
        print('\n\n\n')

    def createChar(self, num, data):
        raise NotImplementedError()


class _VirtualArduino(object):
    def __init__(self, conn=None):
        self.q = Queue.Queue()
        threading.Thread(target=self.run).start()
        self.vals = [512] * 8

    def run(self):
        while True:
            inp = raw_input('u/d/s >>')
            self.q.put(inp)

    def analogRead(self, pin):
        if pin == 0:
            if not self.q.empty():
                s = self.q.get_nowait()
            else:
                s = ''

            if s == 'u':  # up
                return LcdButtonDecoder.BTN_UP

            if s == 'd':  # down
                return LcdButtonDecoder.BTN_DOWN

            if s == 's':  # Select
                return LcdButtonDecoder.BTN_SELECT

            else:
                return LcdButtonDecoder.BTN_NONE

        else:
            n = int(time.time()) / 3
            if n % (pin + 3) == n+1:
                self.vals[pin] += (1 if n % 2 else -1)
                self.vals[pin] = min(1023, max(0, self.vals[pin]))
            return self.vals[pin]


class LCDRow(object):
    """
    A row in the LCD screen
    A non-None on_click indicates a clickable link
    """
    def __init__(self, ui, page, text, normal_prefix = '', selected_prefix='', on_click=None, on_click_largs=None):
        self.ui = ui
        self.page = page
        self.text = text
        self.normal_prefix = normal_prefix
        self.selected_prefix = selected_prefix
        self.on_click = on_click
        self.on_click_largs = on_click_largs or []

    def set_text(self, text):
        self.text = text
        self.page.mark_updated(self)


class LcdPage(list):
    """
    Represents a page - list of rows and its state
    """
    def __init__(self, poll_func=None):
        self.top_line = 0
        self.sel_line = 0
        self.poll_func = poll_func
        list.__init__(self)
        self.updated_indices = set()

    def clear(self):
        self[:] = []
        self.top_line = 0
        self.sel_line = 0

    def mark_updated(self, row):
        try:
            i = self.index(row)
            self.updated_indices.add(i)
        except IndexError:
            pass


class LcdUI(object):
    N_LINES = 2
    N_COLS = 16
    PINS = [8, 9, 4, 5, 6, 7]

    def __init__(self, com_port):
        self._com_port = com_port
        self._simulate = com_port == SIMULATED_ARDUINO_VIRTUAL_PORT
        self.page_stack = [LcdPage()]  # list of pages, top is last

        self._serial_session = None
        self.arduino = None
        self.lcd = None
        self.btn_reader = None
        self.need_update = False
        self.get_arduino_lcd()

        self.menu_sel_char = CustomChar('>', [
            0b00000,
            0b10000,
            0b11000,
            0b11100,
            0b11100,
            0b11000,
            0b10000,
            0b00000
        ])

        self.menu_off_char = CustomChar(' ')

        self.create_custom_chars(self.menu_sel_char, self.menu_off_char)

    def create_custom_chars(self, *chars):
        for c in chars:
            c.create(self.lcd)

    def get_arduino_lcd(self, wait_for_connection=True):
        """
        :return: (adruino, lcd) objects
        """
        if self.arduino and self.lcd:
            logging.error('TODO: Check connection')  # TODO: Check connection
            return self.arduino, self.lcd

        if self._serial_session:
            # noinspection PyBroadException
            try:
                self._serial_session.close()
            except:
                pass

            self._serial_session = None

        if self._simulate:
            self._serial_session = None
            self.arduino, self.lcd = _VirtualArduino(), _VirtualLCD()
        else:
            self._serial_session = SerialManager(self._com_port)
            self.arduino = ArduinoApi(self._serial_session)
            self.lcd = self._init_lcd()

        self.btn_reader = LcdButtonDecoder(self.arduino)

        return self.arduino, self.lcd

    def _init_lcd(self):
        lcd = CustomCharsLcd(self.PINS, [self.N_COLS, self.N_LINES],
                  connection=self._serial_session)

        # for cc in CustomChar.ALL_CHARS:
        #     cc.create_char(lcd)

        return lcd

    def add_text(self, text):
        page = self.page_stack[-1]
        res = LCDRow(self, page, text)
        page.append(res)
        return res

    def add_menu(self, text, on_click, *on_click_largs):
        page = self.page_stack[-1]
        res = LCDRow(self, page, text, str(self.menu_off_char), str(self.menu_sel_char), on_click, on_click_largs)
        page.append(res)
        return res

    def clear(self):
        self.page_stack[-1].clear()

    def push(self, poll_func=None):
        self.page_stack.append(LcdPage(poll_func))

    def pop(self):
        del self.page_stack[-1]
        if not self.page_stack:
            self.push()

    def on_text_updated(self):
        """
        Re-draw the screen
        """
        p = self.page_stack[-1]
        assert isinstance(p, LcdPage)
        if not p:
            self.lcd.clear()
            return

        p.top_line = max(0, min(p.top_line, len(p)-2))
        p.sel_line = max(0, min(p.sel_line, len(p)-2))

        line_n = p.top_line
        for i in range(self.N_LINES):
            row = p[line_n]
            # Move the selection to this line if needed
            if row.on_click and p[p.sel_line].on_click is None and p.sel_line < line_n:
                # or p.sel_line >= p.top_line + self.N_LINES)
                p.sel_line = line_n

            #txt = (row.selected_text or row.text) if line_n == p.sel_line else row.text
            txt = (row.selected_prefix if line_n == p.sel_line else row.normal_prefix) + row.text
            self.lcd.printString('%-16s' % (txt[:16],), 0, i)
            line_n += 1

    def main_loop(self, poll_func=None, poll_freq=2.0, delay=0.05):
        arduino, lcd = self.get_arduino_lcd()
        self.on_text_updated()
        last_poll_time = time.time()
        poll_dt = 1.0 / poll_freq

        while True:
            iter_start_time = time.time()
            btn = self.btn_reader.read_button()

            if btn != LcdButtonDecoder.BTN_NONE:
                if self.handle_button(btn):
                    self.on_text_updated()

            elif iter_start_time >= last_poll_time + poll_dt:
                if poll_func:
                    try:
                        poll_func()
                    except:
                        logging.exception('calling main loop polling')

                if self.page_stack[-1].poll_func:
                    try:
                        self.page_stack[-1].poll_func()
                    except:
                        logging.exception('Calling top page polling')

            if self.page_stack[-1].updated_indices:
                self.on_text_updated()
                self.page_stack[-1].updated_indices.clear()


            if time.time() - iter_start_time > delay:
                time.sleep(delay)

    def handle_button(self, btn):
        """
        Handle button click
        :param btn: LcdButtonDecoder button
        :return: True if handled / changed anything
        """
        p = self.page_stack[-1]

        if btn == LcdButtonDecoder.BTN_UP:
            # Try to find a selection on the
            i = p.sel_line - 1
            while i >= p.top_line:
                if p[i].on_click:
                    p.sel_line = i
                    return True
                i -= 1

            if p.top_line > 0:
                p.top_line -= 1
                if i >= p.top_line and p[i].on_click:
                    p.sel_line = i  # If revealed a link on the top
                return True

            return False

        if btn == LcdButtonDecoder.BTN_DOWN:
            # Try to find a selection on the same page
            i = p.sel_line + 1
            while i < p.top_line + self.N_LINES:
                if p[i].on_click:
                    p.sel_line = i
                    return True
                i += 1

            if p.top_line < len(p) - self.N_LINES:
                p.top_line += 1
                if i < p.top_line + self.N_LINES and p[i].on_click: # If revealed a link on the bottom
                    p.sel_line = i
                return True

        if btn == LcdButtonDecoder.BTN_SELECT:
            sel = p.sel_line
            if sel is not None and p[sel].on_click and p.top_line <= sel < p.top_line + self.N_LINES:
                try:
                    p[sel].on_click(*p[sel].on_click_largs)
                except:
                    pass
            return True

        return False

    def _test_read_kbd(self):
        arduino, lcd = self.get_arduino_lcd()
        self.on_text_updated()
        while True:
            btn = self.btn_reader.read_button()
            if btn != LcdButtonDecoder.BTN_NONE:
                print(btn, time.time())

    def _test_read_kbd_direct(self):
        start_t = time.time()
        readings = []
        for i in range(1000):
            readings.append(self.arduino.analogRead(0))
        dt = time.time() - start_t
        print(len(readings) / dt)
        curr = readings[0]
        n = 0
        for i in readings:
            r = readings[i]
            if r == curr:
                n += 1
            else:
                print('{} x {}'.format(curr, n))
                n = 1
                curr = r
        print('{} x {}'.format(curr, n))


class _Tester(object):
    def __init__(self, comport='COM4'):
        self.ui = LcdUI(comport)
        self.first = None

    def _on_test1(self):
        self.first.set_text(time.ctime()[4:])

    def _on_test2(self):
        self.ui.push()
        self.ui.add_menu('Link 1', self._on_test1)
        self.ui.add_text('Line 2')
        self.ui.add_text('Line 3')
        self.ui.add_text('Line 4')

    def test(self):
        ui = self.ui
        self.first = ui.add_text(time.ctime()[4:])
        ui.add_menu('Link 1', self._on_test1)
        ui.add_text('Line 2')
        ui.add_text('Line 3')
        ui.add_text('Line 4')
        ui.add_menu('Link 5 - push', self._on_test2)
        ui.add_menu('Link 6', self._on_test1)
        ui.add_menu('Link 7', self._on_test1)
        ui.add_text('Line 8')
        ui.add_text('Line 9')

        ui.main_loop()


class Clock:
    # Binary char lines
    CTMB = None
    CT_B = None
    CT__ = None
    C__B = None
    # CDTL = None
    # CDTR = None
    CDOT = None
    C___ = None

    TOP_LINE_CHARS = None
    BTM_LINE_CHARS = None

    def __init__(self, ui):
        self.ui = ui
        self._create_custom_chars()
        self.line1 = ui.add_text('Initialiing...')
        self.line2 = ui.add_text(' ')
        self.last_t = None

    def _create_custom_chars(self):
        if Clock.CTMB:
            return # Already created

        ON_L = 0b11111
        OFF_L = 0b00000
        # DOTL_L = 0b00011
        # DOTR_L = 0b11000
        # DOT_L = 0b011100

        Clock.CTMB = CTMB = CustomChar('|', [ON_L, ON_L, OFF_L, ON_L, ON_L, OFF_L, ON_L, ON_L])
        Clock.CT_B = CT_B = CustomChar('=', [ON_L, ON_L, OFF_L, OFF_L, OFF_L, OFF_L, ON_L, ON_L])
        Clock.CT__ = CT__ = CustomChar('-', [ON_L, ON_L, OFF_L, OFF_L, OFF_L, OFF_L, OFF_L, OFF_L])
        Clock.C__B = C__B = CustomChar('_', [OFF_L, OFF_L, OFF_L, OFF_L, OFF_L, OFF_L, ON_L, ON_L])
        # Clock.CDTL = CustomChar('[', [OFF_L, OFF_L, OFF_L, DOTL_L, DOTL_L, OFF_L, OFF_L, OFF_L])
        # Clock.CDTR = CustomChar(']', [OFF_L, OFF_L, OFF_L, DOTR_L, DOTR_L, OFF_L, OFF_L, OFF_L])
        Clock.CDOT = CustomChar('.') # , [OFF_L, OFF_L, OFF_L, DOT_L, DOT_L, OFF_L, OFF_L, OFF_L])
        Clock.C___ = C___ = CustomChar(' ')

        Clock.TOP_LINE_CHARS = {'0': [CTMB, CT__, CTMB],
                                '1': [CT__, CTMB, C___],
                                '2': [CT_B, CT_B, CTMB],
                                '3': [CT_B, CT_B, CTMB],
                                '4': [CTMB, C__B, CTMB],
                                '5': [CTMB, CT_B, CT_B],
                                '6': [CTMB, CT_B, CT_B],
                                '7': [CT__, CT__, CTMB],
                                '8': [CTMB, CT_B, CTMB],
                                '9': [CTMB, CT_B, CTMB],
                                ' ': [C___, C___, C___]
                                }

        Clock.BTM_LINE_CHARS = {'0': [CTMB, C__B, CTMB],
                                '1': [C__B, CTMB, C__B],
                                '2': [CTMB, C__B, C__B],
                                '3': [C__B, C__B, CTMB],
                                '4': [C___, C___, CTMB],
                                '5': [C__B, C__B, CTMB],
                                '6': [CTMB, C__B, CTMB],
                                '7': [C___, C___, CTMB],
                                '8': [CTMB, C__B, CTMB],
                                '9': [C__B, C__B, CTMB],
                                ' ': [C___, C___, C___]
                                }

        self.ui.create_custom_chars(self.CTMB, self.CT_B, self.CT__, self.C__B, self.CDOT, self.C___)

    def _get_lines(self, hh, mm):
        res = []

        for l_chars in self.TOP_LINE_CHARS, self.BTM_LINE_CHARS:
            l = []
            in_hh = True
            n_line_digits = 0
            for tt in hh, mm:
                ts = ('%-2d' if in_hh else '%02d') % (tt,) # '02' for mins, ' 2' for hours
                for c in ts:
                    l += l_chars[c]
                    n_line_digits += 1
                    if n_line_digits % 2:
                        l.append(self.C___) # put a space after the digit, unless the last one

                if in_hh:
                    l += [self.CDOT, self.CDOT]  # Add dot if rendering the hours
                    in_hh = False

            res.append(''.join(map(str, l))) # Convert each customh char to actual char and join

        return res

    def _test_all_digits(self):

        txt1, txt2 = self._get_lines(12, 34)

        self.line1 = ui.add_text(txt1)
        self.line2 = ui.add_text(txt2)

        txt1, txt2 = self._get_lines(56, 78)

        self.line1 = ui.add_text(txt1)
        self.line2 = ui.add_text(txt2)

        txt1, txt2 = self._get_lines(90, 12)

        self.line1 = ui.add_text(txt1)
        self.line2 = ui.add_text(txt2)

        ui.add_text(''.join([str(x) for x in [self.CTMB, self.CT_B, self.CT__, self.C__B, self.CDOT, self.C___]]))

    def main_loop(self):
        self.ui.add_text('(c)2018 by ZeevK')

        self.ui.main_loop(self.poll)

    def poll(self):
        now = time.localtime()
        hh, mm = now.tm_hour, now.tm_min
        if self.last_t <> (hh, mm):
            txt1, txt2 = self._get_lines(hh, mm)
            self.line1.set_text(txt1)
            self.line2.set_text(txt2)
            self.last_t = hh, mm


def _old():
    '''
    Old emulation test
    :return:
    '''
    try:
        import msvcrt
    except ImportError:
        msvcrt = None
    s = ''
    if msvcrt and msvcrt.kbhit():
        s = ''
        while msvcrt.kbhit():
            s += msvcrt.getch()
    if s == '\xe0H':  # up
        return LcdButtonDecoder.BTN_UP

    if s == '\xe0P':  # down
        return LcdButtonDecoder.BTN_DOWN

    if s == '\r':  # Enter
        return LcdButtonDecoder.BTN_SELECT


if __name__ == '__main__':
    while True:
        try:
            ui = LcdUI('COM4') # SIMULATED_ARDUINO_VIRTUAL_PORT
            Clock(ui).main_loop()
        except:
            time.sleep(3)

    #_Tester().test()
